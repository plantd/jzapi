package com.gitlab.plantd.jzapi;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

/**
 * Message proxy for pubsub networks.
 */
public class Proxy {

  private String backend;
  private String frontend;
  private String capture;
  private ProxyThread thread;

  private class ProxyThread extends Thread {
    private Proxy proxy;
    private boolean running;

    ProxyThread(Proxy proxy) {
      this.proxy = proxy;
      running = false;
    }

    public void run() {
      Thread.currentThread().setName("Proxy");
      try (ZContext context = new ZContext()) {
        ZMQ.Socket xpub = context.createSocket(SocketType.XPUB);
        xpub.bind(proxy.getBackend());
        ZMQ.Socket xsub = context.createSocket(SocketType.XSUB);
        xsub.bind(proxy.getFrontend());
        ZMQ.Socket ctrl = context.createSocket(SocketType.PAIR);
        ctrl.bind(proxy.getCapture());
        ZMQ.proxy(xpub, xsub, null, ctrl);
      }
    }

    void setRunning(boolean running) {
      this.running = running;
    }
  }

  public Proxy() {}

  public Proxy(String backend, String frontend) {
    this.backend = backend;
    this.frontend = frontend;
    capture = "inproc://ctrl-proxy";
  }

  /**
   * Starts the proxy thread.
   */
  public void start() {
    // TODO: implement control socket
    //ZMQ.Socket ctrl = ctx.createSocket(SocketType.PAIR);
    //ctrl.connect("inproc://ctrl-proxy");
    thread = new ProxyThread(this);
    thread.start();
  }

  /**
   * Stops the proxy thread.
   */
  public void stop() throws InterruptedException {
    // TODO: implement shutdown through control socket
    //ctrl.send(ZMQ.PROXY_TERMINATE);
    //ctrl.close();
    thread.setRunning(false);
    thread.join();
  }

  public String getBackend() {
    return backend;
  }

  public void setBackend(String backend) {
    this.backend = backend;
  }

  public String getFrontend() {
    return frontend;
  }

  public void setFrontend(String frontend) {
    this.frontend = frontend;
  }

  public String getCapture() {
    return capture;
  }

  /**
   * Set the capture socket connection endoint.
   *
   * TODO: probably don't ever need to change this value, review this
   *
   * @param capture Capture socket endpoint address
   */
  public void setCapture(String capture) {
    this.capture = capture;
  }
}
