package com.gitlab.plantd.jzapi;

import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Message publisher for connecting to pubsub networks.
 */
public class Source {

  private String endpoint;
  private String envelope;
  private BlockingQueue<String> queue;
  private SourceThread thread;

  private class SourceThread extends Thread {
    private BlockingQueue queue;
    private boolean running;

    SourceThread(BlockingQueue queue) {
      this.queue = queue;
      running = false;
    }

    public void run() {
      Thread.currentThread().setName("Source");

      // Prepare our context and subscriber
      try (ZContext context = new ZContext()) {
        ZMQ.Socket publisher = context.createSocket(SocketType.PUB);
        publisher.connect(endpoint);
        running = true;

        while (running) {
          while (!queue.isEmpty()) {
            try {
              String message = (envelope.equals("")) ? envelope + "" : ":";
              message += queue.take();
              publisher.send(message);
            } catch (InterruptedException ignored) {}
          }
        }
      }
    }

    void setRunning(boolean running) {
      this.running = running;
    }
  }

  /**
   * Default construction.
   */
  public Source() {
    init();
  }

  /**
   * Construction with endpoint and envelope.
   *
   * @param endpoint The connection endpoint
   * @param envelope A subscription envelope
   */
  public Source(String endpoint, String envelope) {
    this.endpoint = endpoint;
    this.envelope = envelope;
    init();
  }

  private void init() {
    queue = new ArrayBlockingQueue<>(1024);
  }

  public void queueMessage(String message) {
    try {
      queue.put(message);
    } catch (InterruptedException ignored) {}
  }

  /**
   * Starts the listening thread.
   */
  public void start() {
    thread = new SourceThread(queue);
    thread.start();
  }

  /**
   * Stops the listening thread.
   */
  public void stop() throws InterruptedException {
    thread.setRunning(false);
    thread.join();
  }

  /**
   * Retrieve the endpoint to connect to.
   *
   * @return The endpoint string
   */
  public String getEndpoint() {
    return endpoint;
  }

  /**
   * Set the connection end point.
   *
   * @param endpoint The endpoint to use
   */
  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  /**
   * Retrieve the envelope to use for subscriptions.
   *
   * @return The envelope string
   */
  public String getEnvelope() {
    return envelope;
  }

  /**
   * Set the subscription envelope.
   *
   * @param envelope The envelope to use
   */
  public void setEnvelope(String envelope) {
    this.envelope = envelope;
  }
}
