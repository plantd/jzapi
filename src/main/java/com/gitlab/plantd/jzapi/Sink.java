package com.gitlab.plantd.jzapi;

import org.zeromq.SocketType;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZContext;

/**
 * Message subscriber for connecting to pubsub networks.
 */
public class Sink {

  private String endpoint;
  private String filter;
  private SinkThread thread;

  private class SinkThread extends Thread {
    private Sink sink;
    private boolean running;

    SinkThread(Sink sink) {
      this.sink = sink;
      running = false;
    }

    public void run() {
      Thread.currentThread().setName("Sink");

      // Prepare our context and subscriber
      try (ZContext context = new ZContext()) {
        Socket subscriber = context.createSocket(SocketType.SUB);
        subscriber.connect(endpoint);
        subscriber.subscribe(filter.getBytes(ZMQ.CHARSET));
        running = true;

        while (running) {
          String message = "";
          // If a filter is set read it first
          if (!filter.equals("")) {
            message = subscriber.recvStr() + ":";
          }

          message += subscriber.recvStr();
          sink.handleMessage(message);
        }
      }
    }

    void setRunning(boolean running) {
      this.running = running;
    }
  }

  /**
   * Default construction.
   */
  public Sink() {}

  /**
   * Construction with endpoint and filter.
   *
   * @param endpoint The connection endpoint
   * @param filter A subscription filter
   */
  public Sink(String endpoint, String filter) {
    this.endpoint = endpoint;
    this.filter = filter;
  }

  /**
   * Handles message data when it arrives. This is meant to be overridden.
   *
   * @param message The message data that was received
   */
  public void handleMessage(String message) {
    System.out.println(message);
  }

  /**
   * Starts the listening thread.
   */
  public void start() {
    thread = new SinkThread(this);
    thread.start();
  }

  /**
   * Stops the listening thread.
   */
  public void stop() throws InterruptedException {
    thread.setRunning(false);
    thread.join();
  }

  /**
   * Retrieve the endpoint to connect to.
   *
   * @return The endpoint string
   */
  public String getEndpoint() {
    return endpoint;
  }

  /**
   * Set the connection end point.
   *
   * @param endpoint The endpoint to use
   */
  public void setEndpoint(String endpoint) {
    this.endpoint = endpoint;
  }

  /**
   * Retrieve the filter to use for subscriptions.
   *
   * @return The filter string
   */
  public String getFilter() {
    return filter;
  }

  /**
   * Set the subscription filter.
   *
   * @param filter The filter to use
   */
  public void setFilter(String filter) {
    this.filter = filter;
  }
}
