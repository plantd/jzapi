# ZeroMQ Devices

Messaging devices to use with `plantd` services.

## Using

### Maven

```xml
<dependency>
  <groupId>com.gitlab.plantd</groupId>
  <artifactId>jzapi</artifactId>
  <version>1.0.1</version>
</dependency>
```

## Packaging

```sh
JAVA_HOME="/usr/lib/jvm/java-8-openjdk" mvn clean deploy
mvn nexus-staging:release
```
